package com.example.salesagent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ImageView;

import com.example.salesagent.Adapters.CustomersAdapter;
import com.example.salesagent.Models.Customer;

import java.util.ArrayList;
import java.util.List;

public class CustomersListUi extends AppCompatActivity {
    private ImageView back_arrow;
    private RecyclerView customersRV;
    CustomersAdapter adapter;
    public static List<Customer> customers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customers_list_ui);

        getSupportActionBar().hide();
        back_arrow = findViewById(R.id.back_arrow);
        customersRV = findViewById(R.id.customersRV);

        back_arrow.setOnClickListener(v -> {
            finish();
        });

        customersRV = findViewById(R.id.customersRV);
        customersRV.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        customersRV.smoothScrollToPosition(0);

        populateSampleCustomers();

        adapter = new CustomersAdapter(getApplicationContext(), customers);
        customersRV.setAdapter(adapter);


    }

    private void populateSampleCustomers() {
        //String name, String email, String phone, String idNo, String gender
        Customer c1 = new Customer("Mulu Kadan", "kadan.md@gmail.com", "+254713919051", "123456789", "Male", "Ag1234");
        Customer c2 = new Customer("Ann Kimani", "ann@gmail.com", "+254712345673", "12340000", "Female","Ag4694");
        Customer c3 = new Customer("Cate Kim", "cate.md@gmail.com", "+254775357", "14436789", "Female","Ag4633");
        Customer c4 = new Customer("James Mui", "jmui@gmail.com", "+25471244054", "123458753", "Male","Ag1234");
        Customer c5 = new Customer("Steve Kip", "skip@gmail.com", "+25472488531", "123458574", "Male","Ag4694");

        customers.add(c1);
        customers.add(c2);
        customers.add(c3);
        customers.add(c4);
        customers.add(c5);
    }
}