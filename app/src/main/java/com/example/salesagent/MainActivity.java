package com.example.salesagent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {
private LinearLayout newCusromer, customersList, Order, Orders;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        newCusromer = findViewById(R.id.newCusromer);
        customersList = findViewById(R.id.customersList);
        Orders = findViewById(R.id.Orders);



        newCusromer.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), CustomerUI.class);
            startActivity(intent);
        });
        customersList.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), CustomersListUi.class);
            startActivity(intent);
        });
        Orders.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), OrdersUi.class);
            startActivity(intent);
        });
    }
}