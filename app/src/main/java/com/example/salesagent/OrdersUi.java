package com.example.salesagent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ImageView;

import com.example.salesagent.Adapters.CustomersAdapter;
import com.example.salesagent.Adapters.ItemsAdapter;
import com.example.salesagent.Models.Customer;
import com.example.salesagent.Models.Item;

import java.util.ArrayList;
import java.util.List;

public class OrdersUi extends AppCompatActivity {
    private ImageView back_arrow;
    private RecyclerView ordersRV;
    ItemsAdapter adapter;

    public static List<Item> orders = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_ui);

        getSupportActionBar().hide();
        back_arrow = findViewById(R.id.back_arrow);
        back_arrow.setOnClickListener(v -> {
            finish();
        });

        ordersRV = findViewById(R.id.ordersRV);
        ordersRV.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        ordersRV.smoothScrollToPosition(0);

        populateSampleItems();

        adapter = new ItemsAdapter(getApplicationContext(), orders);
        ordersRV.setAdapter(adapter);

    }

    private void populateSampleItems() {
//String name, String customerName, String cost, String status
        Item item1 = new Item("Item 1", "Ones Muchoki", "23,334", "Delivered", R.drawable.item1);
        Item item2 = new Item("Item 2", "Ann Muli", "50,331", "Processing", R.drawable.item2);
        Item item3 = new Item("Item 3", "Ones Muchoki", "23,000", "Cancelled", R.drawable.item3);
        Item item4 = new Item("Item 4", "James Lai", "8,432", "New", R.drawable.item4);
        Item item5 = new Item("Item 5", "Ruth Otis", "10,456", "New", R.drawable.item5);

        orders.add(item1);
        orders.add(item2);
        orders.add(item3);
        orders.add(item4);
        orders.add(item5);
    }
}