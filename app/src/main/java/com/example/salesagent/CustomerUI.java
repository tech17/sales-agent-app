package com.example.salesagent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CustomerUI extends AppCompatActivity {
    private ImageView back_arrow;
    private Button registerBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_ui);
        getSupportActionBar().hide();
        back_arrow = findViewById(R.id.back_arrow);
        registerBtn = findViewById(R.id.registerBtn);

        back_arrow.setOnClickListener(v -> {
            finish();
        });
        registerBtn.setOnClickListener(v -> {
            SweetAlertDialog s = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText("Successful!").setConfirmClickListener(
                            sweetAlertDialog -> {
                                finish();
                            }
                    )
                    .setContentText("Customer saved successfully!");


            s.show();
        });
    }
}